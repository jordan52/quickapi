package com.yakbuttertea.quickapi.resources;

import com.google.common.base.Optional;

import com.yammer.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.validator.EmailValidator;

@Path("/validate-email")
@Produces(MediaType.APPLICATION_JSON)
public class EmailValidatorResource {

	public EmailValidatorResource() {
	}

	@GET
	@Timed
	public Boolean validateEmail(@QueryParam("email") Optional<String> email) {
		EmailValidator emailValidator = EmailValidator.getInstance();
		return Boolean.valueOf(emailValidator.isValid(email.get()));
	}
}