package com.yakbuttertea.quickapi;

import com.yakbuttertea.quickapi.health.TemplateHealthCheck;
import com.yakbuttertea.quickapi.resources.EmailValidatorResource;
import com.yakbuttertea.quickapi.resources.QuickResource;
import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;

public class QuickService extends Service<QuickConfiguration> {
    public static void main(String[] args) throws Exception {
        new QuickService().run(args);
    }

    @Override
    public void initialize(Bootstrap<QuickConfiguration> bootstrap) {
        bootstrap.setName("hello-world");
    }

    @Override
    public void run(QuickConfiguration configuration,
                    Environment environment) {
        final String template = configuration.getTemplate();
        final String defaultName = configuration.getDefaultName();
        environment.addResource(new QuickResource(template, defaultName));
        environment.addResource(new EmailValidatorResource());
        environment.addHealthCheck(new TemplateHealthCheck(template));
    }

}
